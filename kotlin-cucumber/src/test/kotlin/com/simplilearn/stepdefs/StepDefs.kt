import io.cucumber.api.PendingException
import io.cucumber.api.java8.En

class StepDefs: En {
    init{
        /*
        When("I have an Undefined Test") {
            throw PendingException()
        }*/

        Then("This test will show as undefined when ran") {
        }

        Given("I want to demonstrate a passing test") {
            assertTrue(true)
        }

        Then("The Test will show as Passing when ran") {
            assertTrue(true)
        }

        Given("I want to demonstrate a failing test") {
            assertTrue(false)
        }

        When("I perform a Verification") {
        }

        Then("The Test will show as Failing when ran") {
            assertTrue(false)
        }
    }
}