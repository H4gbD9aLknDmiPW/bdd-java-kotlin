import io.cucumber.api.CucumberOptions
import io.cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

@RunWith(Cucumber::class)
@CucumberOptions(
    features = ["src/test/resources/com/simplilearn.stepdefs"],
    tags = ["not @ignored"])

class RunKukesTest
